# Mongo

## General
MongoDB is a database used by the FIWARE-components, to save component configuration settings and right-time data. 

Original documentation can be found here: https://docs.mongodb.com/

## Usage of Mongo
Use of this database is handled by the components themselves, so the user does not need know how to operate this database.

Deployment of MongoDB is basic kubernetes statefulset, using the official MongoDB image, so you can read the orignal documentation to learn more: https://docs.mongodb.com/manual/tutorial/getting-started/
## Versioning  
Tested on:  
Version mongo:3.6 (DockerHub digest: sha256:f1ff4f9fd92c45e545c8a9d726123fc1410e2c53c82e99288edf57da98be4e81)

## Volume Usage
Mongo uses a persistent directory to store the data (/data/db)
PVC: NAMESPACE-mongodata-STATEFULSET-NAME

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
There is no outside route to this component.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
UMBRELLA_DEV_IP - This variable contains Umbrella's IP in dev-stage.  
UMBRELLA_PROD_IP - This variable contains Umbrella's IP in production-stage.  
UMBRELLA_STAGING_IP - This variable contains Umbrella's IP in staging-stage.  

### Kubernetes Secrets
This project does not use Kubernetes secrets.

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
